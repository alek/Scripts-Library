param (
    [Parameter(Mandatory)]
    [string]$ArtefactFolder,
    [Parameter(Mandatory)]
    [string]$user,
    [Parameter(Mandatory)]
    [string]$pass
)

$pair = "$($user):$($pass)"

$encodedCreds = [System.Convert]::ToBase64String([System.Text.Encoding]::ASCII.GetBytes($pair))

$basicAuthValue = "Basic $encodedCreds"

$Headers = @{
 Authorization = $basicAuthValue
}
$release = Invoke-WebRequest -Uri 'https://coginov.atlassian.net/rest/api/3/project/15692/versions' -Outfile versiont.json `
-Headers $Headers
Get-Content .\versiont.json

$FILE = Get-Content versiont.json
$FILE | ConvertFrom-Json
$versions = $FILE | ConvertFrom-Json
$versions | Format-Table

foreach ($version in $versions) 
{
   if ($version.released -ne 'true' ) {
  #  write-host "$($version.name)" 
    $v = $($version.name) > v.txt
    
    break
     }
}

$version = Get-Content v.txt
Write-Host $version

$my = $(Get-Date -Format "MM-yyyy")
Write-Host "##vso[task.setvariable variable=buildNumber]$version-$my"

$packageJsonPath = "$ArtefactFolder/package.json"
$packageJson = (Get-Content -Path $packageJsonPath) -replace '\d+\.\d+\.\d+\.\d+',$version
$packageJson | Set-Content -Path "$ArtefactFolder/package.json"
$packageSolutionJsonPath = "$ArtefactFolder/config/package-solution.json"
$packageSolutionJson = (Get-Content -Path $packageSolutionJsonPath) -replace '\d+\.\d+\.\d+\.\d+',$version
$packageSolutionJson | Set-Content -Path "$ArtefactFolder/config/package-solution.json"

$Json = (Get-Content -Path $packageJsonPath)
$solutionJson = (Get-Content -Path $packageSolutionJsonPath)
Write-Host "$Json "
Write-Host "$solutionJson "