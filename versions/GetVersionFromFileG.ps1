param (
    [Parameter(Mandatory)]
    [string]$Folder,
    [Parameter(Mandatory)]
    [string]$VersionFolder,
    [Parameter(Mandatory)]
    [string]$VersionFile
)
$version = Get-Content $Folder/$VersionFolder/$VersionFile
$my = $(Get-Date -Format "MM-yyyy")
Write-Host "##vso[task.setvariable variable=buildNumber]$version-$my"
$Env:buildNumber = "$version"



