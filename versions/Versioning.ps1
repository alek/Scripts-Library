param (
    [Parameter(Mandatory)]
    [string]$ArtefactFolder,
    [Parameter(Mandatory)]
    [string]$user,
    [Parameter(Mandatory)]
    [string]$pass,
    [Parameter(Mandatory)]
    [string]$number
)

$pair = "$($user):$($pass)"

$encodedCreds = [System.Convert]::ToBase64String([System.Text.Encoding]::ASCII.GetBytes($pair))

$basicAuthValue = "Basic $encodedCreds"
write-host "$number"
$Headers = @{
 Authorization = $basicAuthValue
}
$url = "https://coginov.atlassian.net/rest/api/3/project/$number/versions"
write-host "$url"
$release = Invoke-WebRequest -Uri $url -Outfile versiont.json `
-Headers $Headers
Get-Content .\versiont.json

$FILE = Get-Content versiont.json
$FILE | ConvertFrom-Json
$versions = $FILE | ConvertFrom-Json
$versions | Format-Table

foreach ($version in $versions) 
{
   if ($version.released -ne 'true' ) {
  #  write-host "$($version.name)" 
    $v = $($version.name) > v.txt
    
    break
     }
}

$version = Get-Content v.txt
Write-Host $version

$my = $(Get-Date -Format "MM-yyyy")
Write-Host "##vso[task.setvariable variable=buildNumber]$version-$my"
Write-Host "##vso[task.setvariable variable=projectNumber]$version"