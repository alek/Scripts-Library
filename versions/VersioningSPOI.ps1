param (
    [Parameter(Mandatory)]
    [string]$ArtefactFolder

)
Write-host "$($env:projectNumber)"
$version = $($env:projectNumber)
$packageJsonPath = "$ArtefactFolder/package.json"
$packageJson = (Get-Content -Path $packageJsonPath) -replace '\d+\.\d+\.\d+\.\d+',$version
$packageJson | Set-Content -Path "$ArtefactFolder/package.json"
$packageSolutionJsonPath = "$ArtefactFolder/config/package-solution.json"
$packageSolutionJson = (Get-Content -Path $packageSolutionJsonPath) -replace '\d+\.\d+\.\d+\.\d+',$version
$packageSolutionJson | Set-Content -Path "$ArtefactFolder/config/package-solution.json"

$Json = (Get-Content -Path $packageJsonPath)
$solutionJson = (Get-Content -Path $packageSolutionJsonPath)
Write-Host "$Json "
Write-Host "$solutionJson "