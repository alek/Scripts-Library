param (
    [Parameter(Mandatory)]
    [string]$ArtefactFolder
)
##Change directory inside the work directory in the Windows Agent 
cd -Path $ArtefactFolder\_QoreAudit-ASP.NET-Core-CI-Production -PassThru
##Unzip the packageQoreAudit.Desktop.zip
Expand-Archive -LiteralPath $ArtefactFolder\_QoreAudit-ASP.NET-Core-CI-Production\drop\QoreAudit.zip -DestinationPath $ArtefactFolder\_QoreAudit-ASP.NET-Core-CI-Production\QoreAudit
Get-ChildItem -Path $ArtefactFolder\_QoreAudit-ASP.NET-Core-CI-Production\QoreAudit
##Change directory inside the unzip directory in the Windows Agent
cd -Path $ArtefactFolder\_QoreAudit-ASP.NET-Core-CI-Production\QoreAudit -PassThru
## Get the Libraries of QoreAudit and put in text file and clean the empty lines
dir Qore*.dll | Select Name | Out-File library.txt
Get-Content -Path library.txt | where {$_ -ne ''} | out-file library1.txt
$library = $(Get-Content -Path library1.txt)
Expand-Archive -LiteralPath $ArtefactFolder\_QoreAudit-ASP.NET-Core-CI-Production\drop\QoreAudit.API.zip -DestinationPath $ArtefactFolder\_QoreAudit-ASP.NET-Core-CI-Production\QoreAudit.API