param (
    [Parameter(Mandatory)]
    [string]$StorageAccountName,
    [Parameter(Mandatory)]
    [string]$StorageAccountKey,
	[Parameter(Mandatory)]
    [string]$AzureShare,
	[Parameter(Mandatory)]
    [string]$AzureDirectory
	
)

#define variables
$FileName = "QoreAudit $(Get-Date -format yyyy-MM-dd_hhmm).zip"

#create primary region storage context
$ctx = New-AzureStorageContext -StorageAccountName $StorageAccountName -StorageAccountKey $StorageAccountKey


#Check for Share Existence
$S = Get-AzureStorageShare -Context $ctx -ErrorAction SilentlyContinue|Where-Object {$_.Name -eq $AzureShare}

if (!$S.Name)
{
    # create a new share
    $s = New-AzureStorageShare $AzureShare -Context $ctx
}

# Check for directory
$d = Get-AzureStorageFile -Share $s -ErrorAction SilentlyContinue|select Name
if ($d.Name -notcontains $AzureDirectory)
{
    # create a directory in the share
    $d = New-AzureStorageDirectory -Share $s -Path $AzureDirectory
}


#upload the files to the storage

    if($Confirm)
    {
        Set-AzureStorageFileContent -Share $s -Source $FileName -Path $AzureDirectory -Confirm
    }
    else
    {
        Set-AzureStorageFileContent -Share $s -Source $FileName -Path $AzureDirectory -Force
    }


