param (
    [Parameter(Mandatory)]
    [string]$ArtefactFolder,
    [Parameter(Mandatory)]
    [string]$Folder,
    [Parameter(Mandatory)]
    [string]$SubFolder,
    [Parameter(Mandatory)]
    [string]$SubFolder1,
    [Parameter(Mandatory)]
    [string]$FileName,
    [Parameter(Mandatory)]
    [string]$FileName1
)
##Change directory inside the work directory in the Windows Agent 
cd -Path $ArtefactFolder\$Folder -PassThru
##Unzip the packageQoreAudit.Desktop.zip
Expand-Archive -LiteralPath $ArtefactFolder\$Folder\drop\$FileName -DestinationPath $ArtefactFolder\$Folder\$SubFolder
Get-ChildItem -Path $ArtefactFolder\$Folder\$SubFolder
##Change directory inside the unzip directory in the Windows Agent
cd -Path $ArtefactFolder\$Folder\$SubFolder -PassThru
## Get the Libraries of QoreAudit and put in text file and clean the empty lines
dir Qore*.dll | Select Name | Out-File library.txt
Get-Content -Path library.txt | where {$_ -ne ''} | out-file library1.txt
$library = $(Get-Content -Path library1.txt)
Expand-Archive -LiteralPath $ArtefactFolder\$Folder\drop\$FileName1 -DestinationPath $ArtefactFolder\$Folder\$SubFolder1


