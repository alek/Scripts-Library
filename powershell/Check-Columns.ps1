﻿<#
.Synopsis
   This script will delete QoreUltima columns in a document library.
.DESCRIPTION
   This is a maintnance script , It is usful in the case we want to remove all QoreUltima added columns in a document library 
.EXAMPLE
   .\Delete-Columns-Library.ps1 -SubSite "https://tenant.sharepoint.com/sites/SiteCollection" -ListName "Documents" 
#>

Param
(
    #Url of the Root Site of the SharePoint Site Collection
    [Parameter(Mandatory)]
    [string]$SubSite,
    [Parameter(Mandatory)]
    [string]$ListName,
    #Type of authentication with the API
    [PSCredential]$Credentials
)

function Connect-SharePoint {
    param
    (
        [Parameter(Mandatory)]
        [string]$Url,
        [PSCredential]$Credentials
    )

    try {
        if ($Credentials -eq $null) {
            return Connect-PnPOnline -Url $Url -Interactive -ErrorAction Stop
        }
        else {
            return Connect-PnPOnline -Url $Url -Credentials $Credentials -ErrorAction Stop
        }
    }
    catch {
        throw $_
        exit 1
    }
}


Function Column_Exist {
    param 
    (
        [Parameter(Mandatory)]
        [string]$SiteURL,
        [Parameter(Mandatory)]
        [string]$ListName,
        #Type of authentication with the API
        [PSCredential]$Credentials
    ) 
    
    Try {
        $qoreUltimaColumns = @("DocumentId", "IntegrationStatus", "UltimaId", "ClassificationCode", "ClassificationConfidentiality", "ClassificationDescription", "ClassificationId", "ClassificationLevel", "ClassificationTitle", "ClassificationType", "CopyType" , "DatasetId", "Direction", "ErrorDetails", "LifeCycleConfidentiality", "CodeCR", "PlannedDispositionCode", "PlannedDispositionDate", "RetentionRuleCode", "RetentionRuleId", "RetentionRuleTitle", "Status", "UnitId")
        $listFields = Get-PnPField -List $ListName

        foreach ($column in $qoreUltimaColumns) {
            $columnExists = $ListFields | Where-Object { $_.InternalName -eq $column }
            if ($columnExists) {
                Write-host "Column already exist: $column" -ForegroundColor Green
            }
            else {
                Write-host "Column doesn't exist: $column" -ForegroundColor Red
            }
        }
  
        foreach ($column in $qoreUltimaColumns) {
            $field = Get-PnPField -Identity $column -ErrorAction SilentlyContinue
            if ($field -eq $null) {
                Write-host "Site Column doesn't exist: $column" -ForegroundColor Red
            }
        }  
    }
    Catch {
        write-host -f Red "Error Checking Column from List! and site " $_.Exception.Message
    }
}

try {
    Import-Module PnP.Powershell -ErrorAction stop -Force
}
catch {
    Throw $_
    Exit 1
}

write-host "`n"
Write-Host "Check Columns" -ForegroundColor Blue
$Cred = Connect-SharePoint -Url $SubSite -Credentials $Credentials
Column_Exist -SiteURL $SubSite -listName $ListName -Credentials $Cred

