<#
.Synopsis
   This script was created to split folder with the ClassificationCode in the name and put in the ClassificationCode column.
.DESCRIPTION
   - Get the substring from the beginning before the character "-".
   - Removing ClassificationCode from Name
   - Get only if a ClassificationCode was found ont he name
   - Update ClassificationCode column and Name column
.EXAMPLE
   .\Update-Folders-Columns.ps1 -Site "https://example.sharepoint.com/sites/example-1" -FolderSiteRelativeURL "Documents"
#>

#Requires -Modules PnP.PowerShell

param (
    # Site Url
    [Parameter(Mandatory)] 
    [string]$SiteURL,
    # Name of Document Library
    [Parameter(Mandatory)] 
    [string]$FolderSiteRelativeURL
)

cls;


# Connect to SPO using SharePoint PnP PowerShell
Connect-PnPOnline -Url $SiteURL -Interactive

#Get the Folder from site relative URL  
$Folder = Get-PnPFolder -Url $FolderSiteRelativeURL  
       
#Get all Subfolders of a folder - recursively  
$Folders = Get-PnPFolderItem -FolderSiteRelativeUrl $FolderSiteRelativeURL -ItemType Folder -Recursive  

# Iterate through each folder
foreach ($Folder in $Folders) {
    # Get the substring from the beginning before the character "-"
    $ClassificationCode = $Folder.Name.Split("-")[0]
    # Get NewName by removing ClassificationCode from Name
    $NewName = $Folder.Name.Replace("$ClassificationCode-", "")

    if ($ClassificationCode -ne $NewName) {
        # We will get here only if a ClassificationCode was found ont he name, a substring before -
        $IdColValue = $Folder.ListItemAllFields

        # Update ClassificationCode column
        Set-PnPListItem -List $FolderSiteRelativeURL -Identity $IdColValue -Values @{ ClassificationCode = $ClassificationCode }

        # Update the Name column
        Set-PnPListItem -List $FolderSiteRelativeURL -Identity $IdColValue -Values @{ FileLeafRef = $NewName }
    }
}

# Disconnect from SPO
Disconnect-PnPOnline