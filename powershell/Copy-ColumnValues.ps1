<#
.Synopsis
   This script was created to copy the visible text from one column of Lookup content type to another column of Text content type .
.DESCRIPTION
   - Get all the values from a Source column of content type Lookup, separate the value from the visible text and paste it into a Destination column of content type Text in a specific library in SharePoint. 
.EXAMPLE
   .\UpdateColumns.ps1 -Site "https://example.sharepoint.com/sites/example-1" -Library "Documents" -Source "Direction" -Destination "DirectionQSPU"
#>

#Requires -Modules PnP.PowerShell

param (
    # Site Url
    [Parameter(Mandatory)] 
    [string]$Site,
    # Name of Document Library
    [Parameter(Mandatory)] 
    [string]$Library,
    # Internal Name of the source values field
    [Parameter(Mandatory)] # Static Name of the  source values field
    [string]$Source,
    # Internal Name of the destination values field
    [Parameter(Mandatory)]  
    [string]$Destination
)

# Connect to SharePoint
Connect-PnPOnline -Url $Site -Interactive

#Validate the source adn destination fields exist 
$sourceItem = Get-PnPField -List $Library -Identity $source
$destinationItem = Get-PnPField -List $Library -Identity $destination
write-host $items.Name
if ($sourceItem -eq $null -or $destinationItem -eq $null ) {
    Write-Host "The '$sourceItem' column or '$destinationItem' column missing in the '$Library' please check"
    exit 1
}

# Get list of fields in the document library
$items = Get-PnPListItem -List $Library 
foreach ($item in $items) {
    # Get the values of the Lookup content type Source column elements
    $lookupField = $item[$source]
    if ($lookupField -ne $null) {
        # Get the ID and Value elements from the Source column
        $lookupId = $lookupField.LookupId
        $lookupValue = $lookupField.LookupValue

        if ($destinationItem -ne $null) {
            # Copy the reference from the Source column to the Destination column
            $item[$destination] = $lookupField
            Set-PnPListItem -List $library -Identity $item -Values @{ $destination = $lookupValue }
            Write-Host "This visible text: '$lookupValue' in the '$source' column was copied to the '$destination' column"
            
        }
    }
}

# Disconnect from SharePoint
Disconnect-PnPOnline
