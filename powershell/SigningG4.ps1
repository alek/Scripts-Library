﻿param (
    [Parameter(Mandatory)] # Certificate to use for signing from Azure Vault
    [string]$certificateBase64,
    [Parameter(Mandatory)] # Local folder on the agent 
    [string]$baseFolder,
    [Parameter(Mandatory)] # Folder where the artifacts are located 
    [string]$artefactfolder,
    [Parameter(Mandatory)] # Folder where the QoreAudit files are located
    [string]$folderQoreAudit,
    [Parameter(Mandatory)] # Folder where the QoreAudit.API files are located
    [string]$folderQoreAuditAPI,
    [Parameter(Mandatory)] # Name of the files to sign
    [string]$fileName
)

function Load-Certificate($certificateBase64) {
    # Convert certificateBase64 to a certificate
    $buffer = [System.Convert]::FromBase64String($certificateBase64)
    $certificate = [System.Security.Cryptography.X509Certificates.X509Certificate2]::new($buffer)

    if ($certificate -eq $null) {
       throw "Something went wrong while loading the certificate."
    }

    return $certificate
}

# Function to sign a file
function Sign-File($FilePath, $certificate) {
    if (Test-Path $FilePath) {
        Set-AuthenticodeSignature -FilePath $FilePath -Certificate $certificate
        Write-Host "File signed: $FilePath"
    } 
    else {
        Write-Host "The file does not exist: $FilePath"
    }

}

# Assign the value loaded in the "Load-Certificate" function
$certificate = Load-Certificate $certificateBase64

# Set the working location in QoreAudit
$workingFolder = Join-Path -Path $baseFolder -ChildPath "$artefactfolder\$folderQoreAudit"

# Get all .dll and .exe files from QoreAudit
$filesToSign = Get-ChildItem -Path $workingFolder -Recurse -Include "$fileName.dl*" , "$fileName.ex*"

# Sign the dll and exe files of QoreAudit
foreach ($file in $filesToSign) {
   Sign-File $file.FullName $certificate
}

# Set the working location in QoreAudit.API 
$workingFolder = Join-Path -Path $baseFolder -ChildPath "$artefactfolder\$folderQoreAuditAPI"

# Get all .dll and .exe files from QoreAudit.API
$filesToSign = Get-ChildItem -Path $workingFolder -Recurse -Include "$fileName.dl*" , "$fileName.ex*"

# Sign the dll and exe files of QoreAudit.API
foreach ($file in $filesToSign) {
   Sign-File $file.FullName $certificate
}