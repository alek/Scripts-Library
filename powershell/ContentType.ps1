param (
    [Parameter(Mandatory)]
    [string]$Secret,
    [Parameter(Mandatory)]
    [string]$ArtefactFolder,
    #Password
    #[Parameter(Mandatory)]
    #[string]$Password,
    [string]$PatchPackage=(Join-Path -Path $PSScriptRoot -ChildPath "QU-SP-integration.sppkg")
    
)
#$pair = "$($User):$($Pass)"
dir $ArtefactFolder
#$encodedCreds = [System.Convert]::ToBase64String([System.Text.Encoding]::ASCII.GetBytes($pair))
Update-Module -Name PnP.PowerShell
#$BasicAuthValue = "Basic $encodedCreds"
$SecurePassword = ConvertTo-SecureString $Secret -AsPlainText -Force
$UserName = $env:USERNAME
##$Password = [System.Text.Encoding]::UTF8.GetString([System.Convert]::FromBase64String($Secret))
##$Credential = New-Object System.Management.Automation.PSCredential($username, $Password)
$Credentials = New-Object System.Management.Automation.PSCredential($UserName, (ConvertTo-SecureString $Secret -AsPlainText -Force))
Connect-PnPOnline -Url "https://coginovportal.sharepoint.com/$sitecatalog" -Credentials $Credentials
cd $ArtefactFolder
##Invoke-PnPSiteTemplate -Path "QUTemplateContentType.xml"
Add-PnPApp -Path $(System.DefaultWorkingDirectory)/_SharePoint-Integration-CI-Production/drop/drop/QU-SP-integration.sppkg -Overwrite -Scope Site -Publish



Disconnect-PnPOnline