param (
    [Parameter(Mandatory)]
    [string]$UserName,
    [Parameter(Mandatory)]
    [string]$Password,
    [Parameter(Mandatory)]
    [string]$ArtefactFolder,
    [Parameter(Mandatory)]
    [string]$Site,
    [Parameter(Mandatory)]
    [string]$SiteName,
    [Parameter(Mandatory)]
    [string]$ArtefactName,
    [Parameter(Mandatory)]
    [string]$FolderName
)
#Get the variable of working directory and get the security protocol
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
#Download and Install client sharepoint 
$DllUrl = "https://download.microsoft.com/download/E/1/9/E1987F6C-4D0A-4918-AEFE-12105B59FF6A/sharepointclientcomponents_15-4711-1001_x64_en-us.msi"
$dllDestination = "$ArtefactFolder\sp.msi"
Invoke-WebRequest -uri $DllUrl -OutFile $dllDestination
$msiArgument = @(
    "/i", $dllDestination, "/lv*", "$ArtefactFolder\sp.log.txt", "/quiet"
)

$process = Start-Process msiexec -ArgumentList $msiArgument -PassThru -wait -nonewwindow
Write-Host "Exit Code of MSI: $($process.ExitCode )"
if ($process.ExitCode -ne 0)
{
    Exit
    throw "MSI did not install"
}

[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.SharePoint.Client")
[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.SharePoint.Client.Runtime")
Remove-item "$ArtefactFolder\sp.msi", "$ArtefactFolder\sp.log.txt"
#Get Directory in sharepoint 
$SpUrl = "https://coginovportal.sharepoint.com/sites/$Site/"

#Get user and of keyvault

#Get parametres to Upload the packages of QoreAudit and compress then
$List = "Documents"
$FolderUrl = "/sites/QoreCoginov/Shared Documents/$SiteName/Installation-Packages/$FolderName/"
$FileName = "$ArtefactName-Develop.zip"
$archivePath = "$ArtefactFolder\$FileName"

Compress-Archive -Path (Join-Path -Path $ArtefactFolder -ChildPath $ENV:RELEASE_PRIMARYARTIFACTSOURCEALIAS) -DestinationPath $archivePath

try
{
    #Get the Context of the folder
    $Context = New-Object Microsoft.SharePoint.Client.ClientContext($SpUrl)
    $Credentials = New-Object Microsoft.SharePoint.Client.SharePointOnlineCredentials($UserName, (ConvertTo-SecureString $Password -AsPlainText -Force))
    $Context.Credentials = $Credentials
    $folder = $Context.Web.GetFolderByServerRelativeUrl($FolderUrl)
    $Context.Load($folder)
    $Context.ExecuteQuery()

    #Upload the file
    $FileStream = New-Object IO.FileStream($archivePath,[System.IO.FileMode]::Open)
    $FileCreationInfo = New-Object Microsoft.SharePoint.Client.FileCreationInformation
    $FileCreationInfo.Overwrite = $true
    $FileCreationInfo.ContentStream = $FileStream
    $FileCreationInfo.URL = $FileName
    $Upload = $folder.Files.Add($FileCreationInfo)
    $Context.Load($Upload)
    $Context.ExecuteQuery()
    $Upload.CheckOut()
    $Upload.CheckIn("", [Microsoft.SharePoint.Client.CheckinType]::OverwriteCheckIn)
    $Context.ExecuteQuery()
}
catch
{
    throw $_
}
finally
{
    if ($null -ne $Context)
    {
        $Context.Dispose()
    }
    if ($null -ne $FileStream)
    {
        $FileStream.Dispose()
    }
}
