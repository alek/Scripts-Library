﻿<#
.Synopsis
   This script will create the QoreUltima Integration views.
.EXAMPLE
   .\Create-Views.ps1 -Site "https://tenant.sharepoint.com/sites/SiteCollection" -ListName "Documents" 
#>

Param
(
    #Url of the Site
    [Parameter(Mandatory)]
    [string]$Site,
    #Document Library name
    [Parameter(Mandatory)]
    [string]$ListName,
    #Credentials for automation. If left empty, the script will be interactive to authenticate to SharePoint.
    [PSCredential]$Credentials
)

function Connect-SharePoint
{
    param
    (
        [Parameter(Mandatory)]
        [string]$Url,
        [PSCredential]$Credentials
    )

    try
    {
        if ($Credentials -eq $null)
        {
            Connect-PnPOnline -Url $Url -Interactive -ErrorAction Stop
        }
        else
        {
            Connect-PnPOnline -Url $Url -Credentials $Credentials -ErrorAction Stop
        }
    }
    catch
    {
        throw $_
        exit 1
    }
}

Function New-View
{
    param 
    (
        [Parameter(Mandatory)]
        [string]$SiteURL,
        [Parameter(Mandatory)]
        [string]$ListName,
        [Parameter(Mandatory)]
        [string]$ViewName,
        [Parameter(Mandatory)]
        [string]$Query,
        [switch]$SetAsDefault,
        [PSCredential]$Credentials
    ) 

    $ViewFields = @("DocIcon", "LinkFilename","Created","ClassificationCode", "Modified", "Editor", "CodeCR" , "Status","PlannedDispositionCode", "DirectionQUSP","LifeCycleConfidentiality", "IntegrationStatus")
    
    Try 
    {
        #Connect to PnP Online
        Connect-SharePoint -Url $SiteURL -Credentials $Credentials

        $List = Get-PnPList -Identity $ListName -ErrorAction SilentlyContinue
        if($List -eq $Null)
        { 
            Write-Host -f yellow "Invalid List Name!"
            return
        }
        $View = Get-PnPView -List $ListName -Identity $ViewName -ErrorAction SilentlyContinue
        if($View -eq $Null)
        {
            #sharepoint online pnp powershell create view
            Add-PnPView -List $ListName -Title $ViewName -ViewType Html -Fields $ViewFields -Query $Query -ErrorAction Stop -SetAsDefault:$SetAsDefault | Out-Null
            Write-host "View '$ViewName' created Successfully!" -f Green
        }
        else
        {
            Write-Host -f Yellow "View '$ViewName' already exists"
        }
    }
    catch 
    {
        write-host "Error: $($_.Exception.Message)" -foregroundcolor Red
    }
}


try
{
    Import-Module PnP.Powershell -ErrorAction stop -Force
}
catch
{
    Throw $_
    Exit 1
}

write-host "`n"
Write-Host "Creating Views" -ForegroundColor Blue


$enc1 = [System.Text.Encoding]::UTF8.GetBytes("Dossiers archivés dans QoreUltima")
$enc2 = [System.Text.Encoding]::UTF8.GetBytes("Production")
$view1Name= [System.Text.Encoding]::UTF8.GetString($enc1)
$view2Name= [System.Text.Encoding]::UTF8.GetString($enc2)

$query1 = "
    <Where>
        <Eq>
            <FieldRef Name='IntegrationStatus' />
            <Value Type='Text'>archivedInQoreUltima</Value>
        </Eq>
    </Where>"

$query2 = "
    <Where>
        <Neq>
            <FieldRef Name='IntegrationStatus' />
            <Value Type='Text'>archivedInQoreUltima</Value>
        </Neq>
    </Where>" 




New-View -SiteURL $Site  -listName $ListName -ViewName $view1Name -Query $query1 -Credentials $Credentials
New-View -SiteURL $Site  -listName $ListName -ViewName $view2Name -Query $query2 -SetAsDefault -Credentials $Credentials
Write-Host "Views created successfully!" -ForegroundColor Green

# $SiteURL= "https://coginovportal.sharepoint.com/sites/dev-mlsierra02/test01" 
# $ListName= "Documents" 
#Read more: https://www.sharepointdiary.com/2016/05/sharepoint-online-powershell-to-create-list-view.html#ixzz8DDWzs8k7