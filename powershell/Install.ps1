##################################################################
# Configuration steps needed for Development process
##################################################################

# 1- Install SharePoint Online ManagementShell

# 2- Open SharePoint Online ManagementShell as Administrator

# 3- Uninstall SharePointPnPPowerShellOnline using the following command (if it was previously installed)
# Uninstall-Module -Name SharePointPnPPowerShellOnline -AllVersions -Force

# 4- Install new version for PnP.PowerShell using the following command
# Install-Module -Name "PnP.PowerShell"

# 5- Register PnPManagementShellAccess using the following command
# Register-PnPManagementShellAccess

# 6- Connect to SharePoint Online site (SPOS) that will be used to manually create artifacts needed for Ultima2 integration

# 7- Create the following artifacts:
# - Site Columns
# - Content Type(s)
# - Ultima2ConfigurationList
# - Sample Document Library
# - Link previous library to Ultima2 Content Type(s)
# - On previous library create Ultima2 view with required fields and set it  as default view

# 8- From SharePoint Online ManagementShell Connect to SharePoint Online site (SPOS) using the following command
# Connect-PnPOnline SourceTemplateSiteUrl -Interactive # -Interactive parameter is needed if MFA is enabled in the site

# 9- Export template site schema using the following command
# Get-PnPSiteTemplate -Out "Ultima2-Artifacts.xml"

# 10- Open schema file and remove unnecessary nodes
# For now we only need nodes specific to: Site Columns, Content Types and Lists and libraries specific to Ultima2 integration  

##################################################################
# Configuration steps needed for installation process
##################################################################
# 1- Open SharePoint Online ManagementShell as Administrator

# 2- Connect to SharePoint Online site using the following command
Connect-PnPOnline TargetSiteUrl -Interactive # -Interactive parameter is needed if MFA is enabled in the site

# 3- Navigate to folder containing Ultima2-Artifacts.xml

# 4- Import artifacts required for Ultima2 integration using the following command
Invoke-PnPSiteTemplate -Path "Ultima2-Artifacts.xml"