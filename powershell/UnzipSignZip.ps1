param (
    [Parameter(Mandatory)]
    [string]$ArtefactFolder,
    [Parameter(Mandatory)]
    [string]$base64
)
##Change directory inside the work directory in the Windows Agent 
Set-Location -Path $ArtefactFolder\_QoreAudit-ASP.NET-Core-CI-Production -PassThru
##Unzip the packageQoreAudit.Desktop.zip
Expand-Archive -LiteralPath $ArtefactFolder\_QoreAudit-ASP.NET-Core-CI-Production\drop\QoreAudit.zip -DestinationPath $ArtefactFolder\_QoreAudit-ASP.NET-Core-CI-Production\QoreAudit

#################################################################################
Set-Location -Path $ArtefactFolder\_QoreAudit-ASP.NET-Core-CI-Production\QoreAudit -PassThru
## Get the Libraries of QoreAudit and put in text file and clean the empty lines
Get-ChildItem Qore*.dll | Select-Object Name | Out-File library.txt
Get-Content -Path library.txt | Where-Object {$_ -ne ''} | out-file library1.txt
## Signing the libraries of QoreAudit
foreach($line in Get-Content library1.txt | Select-Object -Skip 2 ) {
    if($line -match $regex){
        $file = $line
        $buffer = [System.Convert]::FromBase64String($base64);
        $certificate = [System.Security.Cryptography.X509Certificates.X509Certificate2]::new($buffer);
        Set-AuthenticodeSignature -FilePath $file -Certificate $certificate;
    }
}
## Signing the libraries of QoreAudit
$fileExec = "$ArtefactFolder\_QoreAudit-ASP.NET-Core-CI-Production\QoreAudit\QoreAudit.exe";
$buffer = [System.Convert]::FromBase64String($base64);
$certificate = [System.Security.Cryptography.X509Certificates.X509Certificate2]::new($buffer);
Set-AuthenticodeSignature -FilePath $fileExec -Certificate $certificate;
######################################################################################
##Change directory inside the work directory in the Windows Agent and Zip
Set-Location -Path $ArtefactFolder\_QoreAudit-ASP.NET-Core-CI-Production\ -PassThru
Remove-Item -Path $ArtefactFolder\_QoreAudit-ASP.NET-Core-CI-Production\drop\QoreAudit.zip
##Compress de new package with the sign
Compress-Archive -Path $ArtefactFolder\_QoreAudit-ASP.NET-Core-CI-Production\QoreAudit -DestinationPath $ArtefactFolder\_QoreAudit-ASP.NET-Core-CI-Production\drop\QoreAudit.zip
##Remove folder unzipping, Unzip and Zip scripts 
Remove-Item -Path $ArtefactFolder\_QoreAudit-ASP.NET-Core-CI-Production\QoreAudit -Recurse -Force -EA SilentlyContinue -Verbose
Remove-Item -Path $ArtefactFolder\_QoreAudit-ASP.NET-Core-CI-Production\drop\UnzipSignZip.ps1 -Recurse -Force -EA SilentlyContinue -Verbose
