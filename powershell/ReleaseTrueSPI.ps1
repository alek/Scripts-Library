param (
    [Parameter(Mandatory)]
    [string]$ArtefactFolder,
    [Parameter(Mandatory)]
    [string]$User,
    [Parameter(Mandatory)]
    [string]$Pass
)
$pair = "$($User):$($Pass)"

$encodedCreds = [System.Convert]::ToBase64String([System.Text.Encoding]::ASCII.GetBytes($pair))

$BasicAuthValue = "Basic $encodedCreds"

$Headers = @{
 Authorization = $BasicAuthValue
}

$Release = Invoke-WebRequest -Uri 'https://coginov.atlassian.net/rest/api/3/project/15692/versions' -Outfile versiont.json `
-Headers $Headers
Get-Content .\versiont.json

$File = Get-Content versiont.json
$File | ConvertFrom-Json
$Versions = $File | ConvertFrom-Json
$Versions | Format-Table

foreach ($version in $Versions) 
{
   if ($version.released -ne 'true' ) {
  #  write-host "$($version.name)" 
    $v = $($version.name) > v.txt
    $rel = $($version.self) > rel.txt
    break
     }
Get-Acl
}

$relT = Get-Content rel.txt
Write-Host $version
Write-Host $relT

#####################################################
$Url3 = "$relT";

$contentType3 = "application/json"      
$Headers = @{
 Authorization = $BasicAuthValue
}
$data = @{    
    archived = "false";
    released = "true"    
    };
    
$json = $data | ConvertTo-Json;
Invoke-RestMethod -Method PUT -Uri $Url3 -ContentType $contentType3 -Headers $Headers -Body $json;