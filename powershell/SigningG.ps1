param (
    [Parameter(Mandatory)]
    [string]$ArtefactFolder,
    [Parameter(Mandatory)]
    [string]$base64,
    [Parameter(Mandatory)]
    [string]$Folder,
    [Parameter(Mandatory)]
    [string]$FolderDll,
    [Parameter(Mandatory)]
    [string]$SubFolder,
    [Parameter(Mandatory)]
    [string]$SubFolder1,
    [Parameter(Mandatory)]
    [string]$FileName,
    [Parameter(Mandatory)]
    [string]$FileExec,
    [Parameter(Mandatory)]
    [string]$FileExec1,
    [Parameter(Mandatory)]
    [string]$FileDll,
    [Parameter(Mandatory)]
    [string]$FileDll1
)
Set-Location -Path $ArtefactFolder\$Folder\$FolderDll -PassThru
## Get the Libraries of QoreAudit and put in text file and clean the empty lines
Get-ChildItem $FileName*.dll | Select-Object Name | Out-File library.txt
Get-Content -Path library.txt | Where-Object {$_ -ne ''} | out-file library1.txt
## Signing the libraries of QoreAudit
foreach($line in Get-Content library1.txt | Select-Object -Skip 2 ) {
    if($line -match $regex){
        $File = $Line
        $Buffer = [System.Convert]::FromBase64String($base64);
        $Certificate = [System.Security.Cryptography.X509Certificates.X509Certificate2]::new($buffer);
        Set-AuthenticodeSignature -FilePath $file -Certificate $Certificate;
    }
}
## Signing the exec of QoreAudit
$FileExec = "$ArtefactFolder\$Folder\$SubFolder\$FileExec";
Set-AuthenticodeSignature -FilePath $fileExec -Certificate $Certificate;

## Signing the DLL of QoreAudit
$FileLib = "$ArtefactFolder\$Folder\$SubFolder\$FileDll";
Set-AuthenticodeSignature -FilePath $fileLib -Certificate $Certificate;

## Signing the Exe of QoreAudit.API
$FileExecAPI = "$ArtefactFolder\$Folder\$SubFolder1\$FileExec1";
Set-AuthenticodeSignature -FilePath $fileExecAPI -Certificate $Certificate;

## Signing the DLL of QoreAudit.API
$FileLibAPI = "$ArtefactFolder\$Folder\$SubFolder1\$FileDll1";
Set-AuthenticodeSignature -FilePath $FileLibAPI -Certificate $Certificate;