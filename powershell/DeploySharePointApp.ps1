$username = $env:USERNAME
$password = $env:PASSWORD
m365 login -t password -u $(username) -p $(password)
m365 spo app add --filePath $(System.DefaultWorkingDirectory)/_SharePoint-Integration-CI-Production/drop/drop/QU-SP-integration.sppkg  --overwrite --appCatalogUrl https://coginovportal.sharepoint.com/$(sitecatalog) --appCatalogScope sitecollection
m365 spo app deploy --name QU-SP-integration.sppkg --appCatalogUrl https://coginovportal.sharepoint.com/$(sitecatalog)  --appCatalogScope sitecollection