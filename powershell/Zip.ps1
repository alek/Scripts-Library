param (
    [Parameter(Mandatory)]
    [string]$ArtefactFolder
)
##Change directory inside the work directory in the Windows Agent
cd -Path $ArtefactFolder\_QoreAudit-ASP.NET-Core-CI-Production\ -PassThru
Remove-Item -Path $ArtefactFolder\_QoreAudit-ASP.NET-Core-CI-Production\drop\QoreAudit.zip
Remove-Item -Path $ArtefactFolder\_QoreAudit-ASP.NET-Core-CI-Production\drop\QoreAudit.API.zip
##Compress de new package with the sign
Compress-Archive -Path $ArtefactFolder\_QoreAudit-ASP.NET-Core-CI-Production\QoreAudit -DestinationPath $ArtefactFolder\_QoreAudit-ASP.NET-Core-CI-Production\drop\QoreAudit.zip
Compress-Archive -Path $ArtefactFolder\_QoreAudit-ASP.NET-Core-CI-Production\QoreAudit.API -DestinationPath $ArtefactFolder\_QoreAudit-ASP.NET-Core-CI-Production\drop\QoreAudit.API.zip
##Remove folder unzipping, Unzip and Zip scripts 
Remove-Item -Path $ArtefactFolder\_QoreAudit-ASP.NET-Core-CI-Production\QoreAudit -Recurse -Force -EA SilentlyContinue -Verbose
Remove-Item -Path $ArtefactFolder\_QoreAudit-ASP.NET-Core-CI-Production\QoreAudit.API -Recurse -Force -EA SilentlyContinue -Verbose
Remove-Item -Path $ArtefactFolder\_QoreAudit-ASP.NET-Core-CI-Production\drop\Unzip.ps1 -Recurse -Force -EA SilentlyContinue -Verbose
Remove-Item -Path $ArtefactFolder\_QoreAudit-ASP.NET-Core-CI-Production\drop\Signing.ps1 -Recurse -Force -EA SilentlyContinue -Verbose
Remove-Item -Path $ArtefactFolder\_QoreAudit-ASP.NET-Core-CI-Production\drop\Zip.ps1 -Recurse -Force -EA SilentlyContinue -Verbose