param (
    [Parameter(Mandatory)]
    [string]$ArtefactFolder,
    [Parameter(Mandatory)]
    [string]$Folder,
    [Parameter(Mandatory)]
    [string]$SubFolder,
    [Parameter(Mandatory)]
    [string]$SubFolder1,
    [Parameter(Mandatory)]
    [string]$FileName,
    [Parameter(Mandatory)]
    [string]$FileName1
)
##Change directory inside the work directory in the Windows Agent
cd -Path $ArtefactFolder\$Folder\ -PassThru
Remove-Item -Path $ArtefactFolder\$Folder\drop\$FileName
Remove-Item -Path $ArtefactFolder\$Folder\drop\$FileName1
##Compress de new package with the sign
Compress-Archive -Path $ArtefactFolder\$Folder\$SubFolder -DestinationPath $ArtefactFolder\$Folder\drop\QoreAudit.zip
Compress-Archive -Path $ArtefactFolder\$Folder\$SubFolder1 -DestinationPath $ArtefactFolder\$Folder\drop\QoreAudit.API.zip
##Remove folder unzipping, Unzip and Zip scripts 
Remove-Item -Path $ArtefactFolder\$Folder\$SubFolder -Recurse -Force -EA SilentlyContinue -Verbose
Remove-Item -Path $ArtefactFolder\$Folder\$SubFolder1 -Recurse -Force -EA SilentlyContinue -Verbose
#Remove-Item -Path $ArtefactFolder\$Folder\drop\Unzip.ps1 -Recurse -Force -EA SilentlyContinue -Verbose
#Remove-Item -Path $ArtefactFolder\$Folder\drop\Signing.ps1 -Recurse -Force -EA SilentlyContinue -Verbose
#Remove-Item -Path $ArtefactFolder\$Folder\drop\Zip.ps1 -Recurse -Force -EA SilentlyContinue -Verbose