param (
    [Parameter(Mandatory)]
    [string]$ArtefactFolder,
    [Parameter(Mandatory)]
    [string]$base64
)
Set-Location -Path $ArtefactFolder\_QoreAudit-ASP.NET-Core-CI-Production\QoreAudit -PassThru
## Get the Libraries of QoreAudit and put in text file and clean the empty lines
Get-ChildItem Qore*.dll | Select-Object Name | Out-File library.txt
Get-Content -Path library.txt | Where-Object {$_ -ne ''} | out-file library1.txt
## Signing the libraries of QoreAudit
foreach($line in Get-Content library1.txt | Select-Object -Skip 2 ) {
    if($line -match $regex){
        $file = $line
        $buffer = [System.Convert]::FromBase64String($base64);
        $certificate = [System.Security.Cryptography.X509Certificates.X509Certificate2]::new($buffer);
        Set-AuthenticodeSignature -FilePath $file -Certificate $certificate;
    }
}
## Signing the exec of QoreAudit
$fileExec = "$ArtefactFolder\_QoreAudit-ASP.NET-Core-CI-Production\QoreAudit\QoreAudit.exe";
$buffer = [System.Convert]::FromBase64String($base64);
$certificate = [System.Security.Cryptography.X509Certificates.X509Certificate2]::new($buffer);
Set-AuthenticodeSignature -FilePath $fileExec -Certificate $certificate;

## Signing the Exe of QoreAudit.API
$fileExecAPI = "$ArtefactFolder\_QoreAudit-ASP.NET-Core-CI-Production\QoreAudit.API\QoreAudit.API.exe";
$buffer = [System.Convert]::FromBase64String($base64);
$certificate = [System.Security.Cryptography.X509Certificates.X509Certificate2]::new($buffer);
Set-AuthenticodeSignature -FilePath $fileExecAPI -Certificate $certificate;

## Signing the DLL of QoreAudit.API
$fileLibAPI = "$ArtefactFolder\_QoreAudit-ASP.NET-Core-CI-Production\QoreAudit.API\QoreAudit.API.dll";
$buffer = [System.Convert]::FromBase64String($base64);
$certificate = [System.Security.Cryptography.X509Certificates.X509Certificate2]::new($buffer);
Set-AuthenticodeSignature -FilePath $fileLibAPI -Certificate $certificate;