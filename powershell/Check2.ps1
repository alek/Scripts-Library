Param
(
    #Url of the Root Site of the SharePoint Site Collection
    [Parameter(Mandatory)]
    [string]$SubSite,
    [Parameter(Mandatory)]
    [string]$ListName,
    #Type of authentication with the API
    [PSCredential]$Credentials
)

function Connect-SharePoint {
    param
    (
        [Parameter(Mandatory)]
        [string]$Url,
        [PSCredential]$Credentials
    )

    try {
        if ($Credentials -eq $null) {
            return Connect-PnPOnline -Url $Url -Interactive -ErrorAction Stop
        }
        else {
            return Connect-PnPOnline -Url $Url -Credentials $Credentials -ErrorAction Stop
        }
    }
    catch {
        throw $_
        exit 1
    }
}

Function Column_Exist {
    param 
    (
        [Parameter(Mandatory)]
        [string]$SiteURL,
        [Parameter(Mandatory)]
        [string]$ListName,
        # Tipo de autenticación con la API
        [PSCredential]$Credentials
    ) 
    
    Try {
        $qoreUltimaColumns = @("DocumentId", "IntegrationStatus", "UltimaId", "ClassificationCode", "ClassificationConfidentiality", "ClassificationDescription", "ClassificationId", "ClassificationLevel", "ClassificationTitle", "ClassificationType", "CopyType" , "DatasetId", "Direction", "ErrorDetails", "LifeCycleConfidentiality", "CodeCR", "PlannedDispositionCode", "PlannedDispositionDate", "RetentionRuleCode", "RetentionRuleId", "RetentionRuleTitle", "Status", "UnitId")
        $listFields = Get-PnPField -List $ListName

        foreach ($column in $qoreUltimaColumns) {
            $columnExists = $listFields | Where-Object { $_.InternalName -eq $column }
            if (-not $columnExists) {
                Write-Host "La columna '$column' no existe en la lista '$ListName'." -ForegroundColor Red
            }
        }
  
        foreach ($column in $qoreUltimaColumns) {
            $field = Get-PnPField -Identity $column -ErrorAction SilentlyContinue
            if ($field -eq $null) {
                Write-host "La columna '$column' no existe en el sitio." -ForegroundColor Red
            }
        }  
    }
    Catch {
        Write-host -f Red "Error al verificar las columnas en la lista y el sitio: $($_.Exception.Message)"
    }
}

# Comprueba si PnP PowerShell está disponible y, de ser así, impórtalo.
try {
    Import-Module PnP.Powershell -ErrorAction Stop -Force
}
catch {
    Write-Host -ForegroundColor Red "Error al importar el módulo PnP PowerShell: $($_.Exception.Message)"
    exit 1
}



Write-Host "`n"
Write-Host "Verificar las Columnas" -ForegroundColor Blue
$Cred = Connect-SharePoint -Url $SubSite-Credentials $Credentials
Column_Exist -SiteURL $SubSite-ListName $ListName -Credentials $Cred
