param (
    [Parameter(Mandatory)]
    [string]$ArtefactFolder,
    [Parameter(Mandatory)]
    [string]$user,
    [Parameter(Mandatory)]
    [string]$pass
)

$pair = "$($user):$($pass)"

$encodedCreds = [System.Convert]::ToBase64String([System.Text.Encoding]::ASCII.GetBytes($pair))

$basicAuthValue = "Basic $encodedCreds"

$Headers = @{
 Authorization = $basicAuthValue
}
$release = Invoke-WebRequest -Uri 'https://coginov.atlassian.net/rest/api/3/project/15510/versions' -Outfile versiont.json `
-Headers $Headers
Get-Content .\versiont.json

$FILE = Get-Content versiont.json
$FILE | ConvertFrom-Json
$versions = $FILE | ConvertFrom-Json
$versions | Format-Table

foreach ($version in $versions) 
{
   if ($version.released -ne 'true' ) {
  #  write-host "$($version.name)" 
    $v = $($version.name) > v.txt
    
    break
     }
}

$version = Get-Content v.txt
Write-Host $version

$my = $(Get-Date -Format "MM-yyyy")
Write-Host "##vso[task.setvariable variable=buildNumber]$version-$my"