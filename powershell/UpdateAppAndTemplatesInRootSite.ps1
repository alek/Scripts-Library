﻿<#
.Synopsis
   This script will create the various components necessary for the configuration of the QoreUltima SharePoint Integration application.
.DESCRIPTION
   This script will create the Content Type "QoreUltima" and the Columns for the Group "QUltima" at the Root Site level of the Site Collection in SharePoint.
   It will create the Configuration List used by the QoreUltima SharePoint Integration application.
   It will create the views used by the application in the target library.
.EXAMPLE
   .\Configure-SharePointSite.ps1 -RootSite "https://tenant.sharepoint.com/sites/SiteCollection" -ListName "Documents" -IdentityAPI "https://ultima2restapi/identity" -ContentAPI "https://ultima2restapi/content" -Authentication "AAD"
.EXAMPLE
   .\Configure-SharePointSite.ps1 -RootSite "https://tenant.sharepoint.com/sites/SiteCollection" -ListName "Documents" -SubSite "https://tenant.sharepoint.com/sites/SiteCollection/SubSite" -IdentityRestAPI "https://ultima2restapi/identity" -ContentRestAPI "https://ultima2restapi/content" -Authentication "AAD"
#>

Param
(
    #Url of the Root Site of the SharePoint Site Collection
    [Parameter(Mandatory)]
    [string]$RootSite,
    #Title of the List to add views
    [Parameter(Mandatory)]
    [string]$ListName,
    #Url of the Identity Ultima Rest API
    [Parameter(Mandatory)]
    [string]$IdentityAPI,
    #Url of the Content Ultima Rest API
    [Parameter(Mandatory)]
    [string]$ContentAPI,
    #Type of authentication with the API
    [Parameter(Mandatory)]
    [ValidateSet("BASIC","AAD")]
    [string]$Authentication,
    #Path to QUTemplateContentType.xml. By default, it should be in the same folder as the script
    [string]$ContentTypeTemplate=(Join-Path -Path $PSScriptRoot -ChildPath "QUTemplateContentType.xml"),
    #Path to QUTemplateLibraries.xml. By default, it should be in the same folder as the script
    [string]$LibraryTemplate=(Join-Path -Path $PSScriptRoot -ChildPath "QUTemplateLibraries.xml"),
    #Path to QUTemplateLibraries.xml. By default, it should be in the same folder as the script
    [string]$PatchPackage=(Join-Path -Path $PSScriptRoot -ChildPath "QU-SP-integration.sppkg"),
    #Password
    [Parameter(Mandatory)]
    [string]$Password,
    #WorkingDirectory
    [Parameter(Mandatory)]
    [string]$ArtefactFolder,
    #TemplateLibrarie
    [Parameter(Mandatory)]
    [string]$LibraryT,
    #TemplateContenType
    [Parameter(Mandatory)]
    [string]$ContentTypeT
)
$User = $env:USERNAME
$SecurePassword = ConvertTo-SecureString $Password -AsPlainText -Force
$Credentials = New-Object System.Management.Automation.PSCredential($User, (ConvertTo-SecureString $Password -AsPlainText -Force))
Connect-PnPOnline -Url $RootSite -Credentials $Credentials
cd $ArtefactFolder/
Function New-View
{
    param 
    (
        [Parameter(Mandatory)]
        [string]$SiteURL,
        [Parameter(Mandatory)]
        [string]$ListName,
        [Parameter(Mandatory)]
        [string]$ViewName,
        [Parameter(Mandatory)]
        [string]$Query,
        [switch]$SetAsDefault,
        [PSCredential]$Credentials
    ) 

    $ViewFields = @("DocIcon", "LinkFilename","Created","ClassificationCode", "Modified", "Editor", "CodeCR" , "Status","PlannedDispositionCode", "Direction","LifeCycleConfidentiality", "IntegrationStatus")
    
    Try 
    {
        $List = Get-PnPList -Identity $ListName -ErrorAction SilentlyContinue
        if($List -eq $Null)
        { 
            Write-Host -f yellow "Invalid List Name!"
            return
        }
        $View = Get-PnPView -List $ListName -Identity $ViewName -ErrorAction SilentlyContinue
        if($View -eq $Null)
        {
            #sharepoint online pnp powershell create view
            Add-PnPView -List $ListName -Title $ViewName -ViewType Html -Fields $ViewFields -Query $Query -ErrorAction Stop -SetAsDefault:$SetAsDefault | Out-Null
            Write-host "View '$ViewName' created Successfully!" -f Green
        }
        else
        {
            Write-Host -f Yellow "View '$ViewName' already exists"
        }
    }
    catch 
    {
        write-host "Error: $($_.Exception.Message)" -foregroundcolor Red
    }
}

Function Set-ContentType
{
    param 
    ( 
        [Parameter(Mandatory)]
        [string]$RootURL,
        [Parameter(Mandatory)]
        [string]$SiteURL,
        [Parameter(Mandatory)]
        [string]$ListName,
        [PSCredential]$Credentials
    )
    $ContentTypeName = "QoreUltima"
    Try 
    {
        #Get ContentType of QoreUltima
        $ContentType = Get-PnPContentType -Identity $ContentTypeName
        $ListContentType = Get-PnPContentType -list $ListName -Identity $ContentTypeName -ErrorAction SilentlyContinue

        if (!$ContentType ) 
        {
            Write-host -f Yellow "Could Not Find Content Type:"$ContentTypeName
            return
        }
        if ($ListContentType -ne $Null) 
        {
            Write-host -f Yellow "The content type '$ContentTypeName' was already associated"
            return
        }
        #Add Content Type to Library
        Add-PnPContentTypeToList -List $ListName -ContentType $ContentType.Id
    }
    Catch 
    {
        write-host "Error: $($_.Exception.Message)" -foregroundcolor Red
    }


#Read more: https://www.sharepointdiary.com/2016/06/sharepoint-online-add-content-type-to-list-library-using-powershell.html#ixzz8DIEYHHKr
}


try
{
    Import-Module PnP.Powershell -ErrorAction stop -Force
}
catch
{
    Throw $_
    Exit 1
}

if ([string]::IsNullOrWhiteSpace($SubSite))
{
    $SubSite = $RootSite
}

if ($IdentityAPI.EndsWith("/"))
{
    $IdentityAPI = $IdentityAPI.Substring(0, $IdentityAPI.Length-1)
}

if ($ContentAPI.EndsWith("/"))
{
    $ContentAPI = $ContentAPI.Substring(0, $ContentAPI.Length-1)
}

Write-Host "Importing Content Types and Site Columns" -ForegroundColor Blue
Invoke-PnPSiteTemplate -Path $ContentTypeT -ErrorAction Stop
Write-Host "Content Types and Site Columns imported successfully!" -ForegroundColor Green

write-host "`n"
Write-Host "Creating Configuration List" -ForegroundColor Blue
Invoke-PnPSiteTemplate -Path $LibraryT -Parameters @{"IdentityApiUrl"= $IdentityAPI;"AuthenticationMethod"= $Authentication;"ContentApiUrl"= $ContentAPI} -ErrorAction Stop
Write-Host "Configuration List created successfully!" -ForegroundColor Green

write-host "`n"
Write-Host "Associating QoreUltima Content Type to Site" -ForegroundColor Blue
Set-ContentType -RootURL $rootSite -SiteURL $SubSite -listName $ListName -Credentials $Credentials
Write-Host "Association completed!" -ForegroundColor Green

write-host "`n"
Write-Host "Creating Views" -ForegroundColor Blue


$enc1 = [System.Text.Encoding]::UTF8.GetBytes("Dossiers archivés dans QoreUltima")
$enc2 = [System.Text.Encoding]::UTF8.GetBytes("Production")
$view1Name= [System.Text.Encoding]::UTF8.GetString($enc1)
$view2Name= [System.Text.Encoding]::UTF8.GetString($enc2)

$query1 = "
    <Where>
        <Eq>
            <FieldRef Name='IntegrationStatus' />
            <Value Type='Text'>archivedInQoreUltima</Value>
        </Eq>
    </Where>"

$query2 = "
    <Where>
        <Neq>
            <FieldRef Name='IntegrationStatus' />
            <Value Type='Text'>archivedInQoreUltima</Value>
        </Neq>
    </Where>" 

New-View -SiteURL $SubSite  -listName $ListName -ViewName $view1Name -Query $query1 -Credentials $Credentials
New-View -SiteURL $SubSite  -listName $ListName -ViewName $view2Name -Query $query2 -SetAsDefault -Credentials $Credentials
Write-Host "Views created successfully!" -ForegroundColor Green

#Upload, Overwrite and Deploy Integration-Sharepoint-App in RootSite 
#Add-PnPApp -Path $PatchPackage -Overwrite -Scope Site -Publish

#Read more: https://www.sharepointdiary.com/2016/05/sharepoint-online-powershell-to-create-list-view.html#ixzz8DDWzs8k7