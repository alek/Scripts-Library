param (
    [Parameter(Mandatory)]
    [string]$username,
    [Parameter(Mandatory)]
    [string]$password,
    [Parameter(Mandatory)]
    [string]$ArtefactFolder
)
#Get the variable of working directory and get the security protocol
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
#Download and Install client sharepoint 
$dllUrl = "https://download.microsoft.com/download/E/1/9/E1987F6C-4D0A-4918-AEFE-12105B59FF6A/sharepointclientcomponents_15-4711-1001_x64_en-us.msi"
$dllDestination = "$ArtefactFolder\sp.msi"
Invoke-WebRequest -uri $dllUrl -OutFile $dllDestination
$msiArgument = @(
    "/i", $dllDestination, "/lv*", "$ArtefactFolder\sp.log.txt", "/quiet"
)

$process = Start-Process msiexec -ArgumentList $msiArgument -PassThru -wait -nonewwindow
Write-Host "Exit Code of MSI: $($process.ExitCode )"
if ($process.ExitCode -ne 0)
{
    Exit
    throw "MSI did not install"
}

[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.SharePoint.Client")
[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.SharePoint.Client.Runtime")
#Get Directory in sharepoint 
$spUrl = "https://coginovportal.sharepoint.com/sites/QoreCoginov/"

#Get user and of keyvault
##$Username = "$(deplyQAU)"
##$Password = "$(deployQAP)"
#Get parametres to upload the packages of QoreAudit and compress then
$List = "Documents"
$folderUrl = "/sites/QoreCoginov/Shared Documents/QoreAudit/Installation-Packages"
$fileName = "QoreAudit $(Get-Date -format yyyy-MM-dd_hhmm).zip"
$archivePath = "$ArtefactFolder\$fileName"

Compress-Archive -Path (Join-Path -Path $ArtefactFolder -ChildPath $ENV:RELEASE_PRIMARYARTIFACTSOURCEALIAS) -DestinationPath $archivePath

try
{
    #Get the context of the folder
    $context = New-Object Microsoft.SharePoint.Client.ClientContext($spUrl)
    $credentials = New-Object Microsoft.SharePoint.Client.SharePointOnlineCredentials($Username, (ConvertTo-SecureString $Password -AsPlainText -Force))
    $context.Credentials = $credentials
    $folder = $context.Web.GetFolderByServerRelativeUrl($folderUrl)
    $context.Load($folder)
    $context.ExecuteQuery()

    #Upload the file
    $fileStream = New-Object IO.FileStream($archivePath,[System.IO.FileMode]::Open)
    $fileCreationInfo = New-Object Microsoft.SharePoint.Client.FileCreationInformation
    $fileCreationInfo.Overwrite = $false
    $fileCreationInfo.ContentStream = $fileStream
    $fileCreationInfo.URL = $fileName
    $upload = $folder.Files.Add($fileCreationInfo)
    $context.Load($upload)
    $context.ExecuteQuery()

    #Using the checkout/checkin to avoid generating 2 versions in document library
    if($upload.CheckOutType -ne "None"){
        $upload.CheckIn("Checked in by Automation", [Microsoft.SharePoint.Client.CheckinType]::MajorCheckIn)
    }

    $upload.CheckOut()
    $upload.CheckIn("", [Microsoft.SharePoint.Client.CheckinType]::OverwriteCheckIn)
    $context.ExecuteQuery()
}
catch
{
    throw $_
}
finally
{
    if ($null -ne $context)
    {
        $context.Dispose()
    }
    if ($null -ne $fileStream)
    {
        $fileStream.Dispose()
    }
}
